proyecto_recordatoriosAPP

# **INSTALACIÓN, PLANEACIÓN Y CONFIGURACIÓN DE HERRAMIENTAS.**

# Instalar Visual Studio Code

1. Descargar Visual Studio Code en su página oficial y elegir el sistema operativo deseado: https://code.visualstudio.com/

2. Abrir el archivo descargado.

3. Seguir las instrucciones del asistente de instalación.

4. Aceptar los términos y condiciones.

5. Seleccionar las opciones que prefieran (como agregar al PATH y crear un icono en el escritorio).

6. Haz clic en "Instalar".

# Configurar Visual Studio Code

1. Abrir VS Code.

2. Ir a Extensiones (icono de cuatro cuadrados en la barra lateral izquierda).

3. Busca e instala las extensiones Flutter y Dart.

4. Abre la terminal integrada (View > Terminal).

5. Ejecuta flutter create nombre_del_proyecto para crear un nuevo proyecto de Flutter.

# Instalar Android Studio

1. Descargar Android Studio en su página oficial y elegir el sistema operativo deseado: https://developer.android.com/studio?hl=es-419

2. Abrir el archivo descargado.

3. Seguir las instrucciones del asistente de instalación.

4. Aceptar los términos y condiciones.

5. Elegir la configuración estándar recomendada.

6. Selecciona la instalación del SDK de Android y el emulador.

7. Haz clic en "Finalizar" una vez que la instalación se haya completado.

# Configurar Android Studio

1. Instala los complementos de Flutter y Dart desde el menú de configuraciones: File > Settings > Plugins y busca e instala Flutter (esto también instalará Dart).
